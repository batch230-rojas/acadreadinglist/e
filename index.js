let numA = 2;
let numB = 3;
let sum = numA + numB;
console.log("The sum of numA and numB should be " + sum);
console.log("Code Output: " + sum);
console.log("-------------");

let numC = 6;
let numD = 5;
let difference = numC - numD;
console.log("The difference of numA minus numB should " + difference);
console.log("Code Output: " + difference);
console.log("-------------");

let numE = 4;
let numF = 3;
let product = numE * numF;
console.log("The product of numA times numB should be " + product);
console.log("Code Output: " + product);
console.log("-------------");

let numG = 6;
let numH = 3;
let quotient = numG / numH;
console.log("The quotient of numA divided by numB should be " + quotient);
console.log("Code Output: " + quotient);
console.log("-------------");
